var _ = require("underscore"),
  express = require("express"),
  router = express.Router(),
  async = require("async"),
  fs = require("fs"),
  request = require("request"),
  cheerio = require("cheerio"),
  db = require("../db"),
  redis = require("../lib/redisclient"),
  settings = require("../settings"),
  redis_prefix = require("../../package").name,
  version = require("../../package").version,
  ensureAuthenticated = require("../lib/middleware").ensureAuthenticated;

//libs
function set_tags(tagstring) {
  if (tagstring) {
    if (_.isArray(tagstring)) {
      return tagstring;
    } else {
      return _.map(tagstring.split(","), function (tag) {
        return tag.trim();
      });
    }
  } else {
    return [];
  }
}

router.route("/links/*").get(ensureAuthenticated, async (req, res, next) => {
  const tags = req.params[0].split("/").filter((tag) => tag.length > 0);
  const page = parseInt(req.query.page, 10) || 0;
  const per_page = 50;
  const q_select_links =
    "select * from links where creator = $1 and tags @> $2 order by created desc limit $3 offset $4";
  try {
    const link_result = await db.q(q_select_links, [
      req.user.user_id,
      tags,
      per_page,
      per_page * page,
    ]);
    res.json({ links: link_result.rows });
  } catch (error) {
    console.error(error);
    return res.status(500);
  }
});

router.route("/tags/*").get(ensureAuthenticated, async (req, res, next) => {
  var id = redis_prefix + "_tags_" + req.user.user_id;
  redis.client.zrevrangebyscore(
    id,
    "+inf",
    1,
    "withscores",
    "limit",
    0,
    20,
    (err, tags_list) => {
      var tags = [];
      for (var i = 0; i < tags_list.length; i += 2) {
        tags.push({ text: tags_list[i], score: tags_list[i + 1] });
      }
      res.json({ tags });
    }
  );
});

router
  .route("/")
  .get(async function (req, res, next) {
    async.parallel(
      {
        tag_count: function (callback) {
          redis.client.zcard(redis_prefix + "_tags", function (err, tag_count) {
            callback(err, tag_count);
          });
        },
        url_count: function (callback) {
          redis.client.zcard(redis_prefix + "_urls", function (err, url_count) {
            callback(err, url_count);
          });
        },
        tags: function (callback) {
          redis.client.zrevrange(redis_prefix + "_tags", 0, 9, function (err, tags) {
            callback(err, tags);
          });
        },
        urls: function (callback) {
          redis.client.zrevrange(redis_prefix + "_urls", 0, 9, function (err, urls) {
            callback(err, urls);
          });
        },
      },
      function (err, results) {
        if (err) {
          return next(err);
        }
        res.format({
          json: function () {
            res.json(200, {
              url: settings.uri,
              urls: results.urls || [],
              tags: results.tags || [],
              url_count: results.url_count,
              tag_count: results.tag_count,
            });
          },
        });
      }
    );
  })
  .post(ensureAuthenticated, async (req, res, next) => {
    const { url, title, description, tags, private } = req.body;
    const q_insert_link =
      "insert into links (url, creator, created, title, description, private, tags) values ($1, $2, $3, $4, $5, $6, $7)";
    const q_select_link =
      "select link_id, url, creator, created, title, description, private, tags from links where creator = $1 and url = $2";
    try {
      await db.q(q_insert_link, [
        url,
        req.user.user_id,
        new Date(),
        title,
        description,
        private,
        tags.split(",").map((tag) => tag.trim()),
      ]);
      const link_result = await db.q(q_select_link, [req.user.user_id, url]);
      res.json(link_result.rows[0]);
    } catch (error) {
      console.error(error);
    }
  })
  .patch(ensureAuthenticated, async (req, res, next) => {
    const q_select_link =
      "select link_id, url, creator, created, title, description, private, tags from links where creator = $1 and link_id = $2";
    const q_update_link =
      "update links set title=$3, description=$4, private=$5, tags=$6 where creator = $1 and link_id = $2";
    try {
      const old_link_result = await db.q(q_select_link, [
        req.user.user_id,
        req.body.link_id,
      ]);
      const [old_link] = old_link_result.rows;
      const { title, description, private, tags } = req.body;
      await db.q(q_update_link, [
        req.user.user_id,
        req.body.link_id,
        title,
        description,
        private,
        tags,
      ]);
      const link_result = await db.q(q_select_link, [
        req.user.user_id,
        req.body.link_id,
      ]);
      if (!old_link.private) {
        old_link.tags.forEach((tag) => {
          redis.client.zincrby(redis_prefix + "_tags", -1, tag);
          redis.client.zincrby(redis_prefix + "_tags_" + req.user.user_id, -1, tag);
        });
      }
      if (!private) {
        tags.map((tag) => {
          redis.client.zincrby(redis_prefix + "_tags", 1, tag);
          redis.client.zincrby(redis_prefix + "_tags_" + req.user.user_id, 1, tag);
        });
      }
      return res.json(link_result.rows[0]);
    } catch (error) {
      console.error(error);
    }
  })
  .delete(ensureAuthenticated, async (req, res, next) => {
    const q_select_link =
      "select link_id, url, creator, created, title, description, private, tags from links where creator = $1 and link_id = $2";
    const q_delete_link =
      "delete from links where creator = $1 and link_id = $2";
    try {
      const link_result = await db.q(q_select_link, [
        req.user.user_id,
        req.body.link_id,
      ]);
      const link = link_result.rows[0];
      await db.q(q_delete_link, [req.user.user_id, req.body.link_id]);
      if (!link.private) {
        redis.client.zincrby(redis_prefix + "_urls", -1, link.url);
        _.each(link.tags, function (tag) {
          redis.client.zincrby(redis_prefix + "_tags", -1, tag);
          redis.client.zincrby(redis_prefix + "_tags_" + req.user._id, -1, tag);
        });
      }
      return res.json({ status: true });
    } catch (error) {
      console.error(error);
    }
  });

var fetch_title = function (url, callback) {
  if (url.indexOf("http") !== 0) {
    url = "https://" + url;
  }

  request(
    {
      url: url,
      gzip: true,
      headers: {
        "User-Agent": "Rememberthelink/" + version,
      },
    },
    function (err, response, body) {
      if (err) {
        return callback(err);
      }
      if (!body) {
        return callback(new Error(res.locals.__("Could not fetch page")));
      }
      var $ = cheerio.load(body);
      var title = $("html head title").text().trim();

      // For weird pages ...
      if (!title) {
        title = $("title").first().text().trim() || "";
      }
      callback(null, url, title);
    }
  );
};

router.get("/title", function (req, res, next) {
  var url = req.query.url;

  if (!url) {
    return next(new Error(res.locals.__("Url missing.")));
  }

  fetch_title(url, function (err, url, title) {
    if (err) {
      return res.status(400).json({ error: err.message });
    }
    res.status(200).json({ title: title, url: url });
  });
});

module.exports = router;
