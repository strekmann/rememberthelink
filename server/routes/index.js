var express = require("express"),
  router = express.Router(),
  db = require("../db"),
  ensureAuthenticated = require("../lib/middleware").ensureAuthenticated;

router.get("/login", function (req, res, next) {
  res.render("login");
});

router.get("/logout", function (req, res, next) {
  req.logout();
  req.session.destroy();
  res.redirect("/");
});

router
  .route("/account")
  .all(ensureAuthenticated)
  .get(function (req, res, next) {
    res.format({
      json: function () {
        res.status(200).json(200, {
          user: req.user,
        });
      },
      html: function () {
        res.render("account");
      },
    });
  })
  .put(async function (req, res, next) {
    req.assert("name", "name is required").notEmpty();

    var errors = await req.getValidationResult();
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const q_update_user = "update users set name=$2 where user_id=$1";
    const q_select_updated_user = "select user_id, name, email, google_id, created, is_active, is_admin from users where user_id=$1";
    try {
      await db.q(q_update_user, [req.user.user_id, req.body.name]);
      const user_result = await db.q(q_select_updated_user, [req.user.user_id]);
      if(user_result) {
        return res.status(200).json({user: user_result.rows[0]});
      }
      return res.status(404);
    } catch(error) {
      next(error);
    }
  });

module.exports = router;
