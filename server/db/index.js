const { Pool } = require("pg");

const connectionString = process.env.POSTGRES_URI;

const pool = new Pool({ connectionString });

module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback);
  },
  q: (text, params) => pool.query(text, params),
};
