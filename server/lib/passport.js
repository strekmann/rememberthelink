var shortid = require("short-mongo-id"),
  passport = require("passport"),
  GoogleStrategy = require("passport-google-oauth").OAuth2Strategy,
  db = require("../db");

module.exports = function (app) {
  passport.serializeUser(function (user, done) {
    done(null, user.user_id);
  });

  passport.deserializeUser(function (id, done) {
    db.query("select * from users where user_id = $1", [id], (err, users) => {
      if (err) {
        return done(err.message, null);
      }
      if (users.rowCount === 0) {
        return done("Could not find user with id" + id);
      }
      done(null, users.rows[0]);
    });
  });

  passport.use(
    new GoogleStrategy(
      {
        clientID: app.conf.auth.google.clientId,
        clientSecret: app.conf.auth.google.clientSecret,
        callbackURL: app.conf.auth.google.callbackURL,
      },
      function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
          const query_select_viewer =
            "select * from users where google_id = $1";
          db.query(query_select_viewer, [profile.id], (err, users) => {
            if (err) {
              return done(err.message, null);
            }
            if (users.rowCount === 1) {
              return done(null, users.rows[0]);
            } else {
              db.query(
                "insert into users (id, name, email, google_id, is_admin, is_active) values ($1, $2, $3, $4, $5, $6)",
                [
                  shortid(),
                  profile.displayName,
                  profile._json.email,
                  profile.id,
                  false,
                  true,
                ],
                (err) => {
                  if (err) {
                    return done("Could not create user: " + err);
                  }
                  db.query("commit", (err) => {
                    if (err) {
                      return done("Could not commit: " + err);
                    }
                    db.query(
                      query_select_viewer,
                      [profile.id],
                      (err, users) => {
                        if (err || users.rowCount === 0) {
                          return done(
                            "Could not find newly created user. This is obviously an error that should never happen"
                          );
                        }
                        return users.rows[0];
                      }
                    );
                  });
                }
              );
            }
          });
        });
      }
    )
  );

  /*
  if (process.env.NODE_ENV === "test") {
    var LocalStrategy = require("passport-local").Strategy;

    passport.use(
      new LocalStrategy(function (username, password, done) {
        User.findOne({ username: username.toLowerCase() }, function (
          err,
          user
        ) {
          if (err) {
            return done(err);
          }
          if (!user) {
            return done(null, false, { message: "Unrecognized username." });
          }
          var hashedPassword = crypto.createHash(user.algorithm);
          hashedPassword.update(user.salt);
          hashedPassword.update(password);
          if (user.password === hashedPassword.digest("hex")) {
            return done(null, user);
          } else {
            return done(null, false, { message: "Incorrect password." });
          }
        });
      })
    );
  }
  */

  return passport;
};
