var redis = require("redis");
const promisify = require("util").promisify;
var host = process.env.REDIS_HOST || "127.0.0.1";
var client = redis.createClient({ host });
const keys = promisify(client.keys).bind(client);
const del = promisify(client.del).bind(client);
const zadd = promisify(client.zadd).bind(client);
if (process.env.NODE_ENV === "test") {
  client.select(1);
}
module.exports = {client, keys, del, zadd };
