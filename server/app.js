var express = require("express"),
  path = require("path"),
  moment = require("moment"),
  //multer      = require('multer'),
  settings = require("./settings"),
  app = require("libby")(express, settings);

// # Application setup
// Add passport to application.
app.passport = require("./lib/passport")(app);

if (app.settings.env === "development") {
  // pretty print jade html in development
  app.locals.pretty = true;
}

// Use jade templates located under server/views
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// Initialize passport
app.use(app.passport.initialize());
app.use(app.passport.session());
//app.use(multer());

// Make some variables always available in templates.
app.use(function (req, res, next) {
  res.locals.active_user = req.user;
  res.locals.stamp = app.stamp;
  res.locals.longdate = function (date) {
    if (!date) {
      return;
    }
    return moment(date).format("LLL");
  };
  res.locals.shortdate = function (date) {
    if (!date) {
      return;
    }
    return moment(date).format("Do MMM");
  };
  next();
});

// ## Application routes
// Authentication against google.
app.get(
  "/auth/google",
  app.passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
    ],
  }),
  function (req, res) {}
);
app.get(
  "/auth/google/callback",
  app.passport.authenticate("google", { failureRedirect: "/login" }),
  function (req, res) {
    var url = req.session.returnTo || "/";
    delete req.session.returnTo;
    res.redirect(url);
  }
);

// Core routes like index, login, logout and account.
app.use("/api", require("./routes/index"));
app.use("/api", require("./routes/links"));
//app.use("/api/profile", require("./routes/profile"));
//app.use("/api/friends", require("./routes/friends"));
//app.use("/api/admin", require("./routes/admin"));
//app.use("/api/bookmarks", require("./routes/bookmarks"));

// Static file middleware serving static files.
app.use(express.static(path.join(__dirname, "..", "public")));

// Internal server error - 500 status
app.use(function (err, req, res, next) {
  console.error(
    "ERROR: %s [%s] %s",
    req.ip,
    new Date().toString(),
    err.message || err
  );
  console.error(err.stack);

  res.status(500);
  res.format({
    html: function () {
      res.render("500", {
        error: err.message,
        status: err.status || 500,
      });
    },

    json: function () {
      res.json(500, {
        error: err.message,
        status: err.status || 500,
      });
    },
  });
});

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "public", "index.html"));
});

// Export application.
module.exports = app;
