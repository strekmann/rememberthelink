#!/usr/bin/env node

var redis = require("../server/lib/redisclient"),
  db = require("../server/db"),
  prefix = require("../package").name;

async function redisUpdate() {
  try{
  const keys = await redis.keys(prefix + "_*");
  for (const key of keys) {
    redis.del(key);
  }
} catch (error) {
  console.error(error);
}

  var tags = {},
    urls = {},
    user_tags = {};

  const q_select_links =
    "select url, creator, tags from links where private != false";
  try {
    const link_result = await db.q(q_select_links);
    for (const link of link_result.rows) {
      urls[link.url] = urls[link.url] || 0;
      urls[link.url] += 1;
      for (const tag of link.tags) {
        tags[tag] = tags[tag] || 0;
        tags[tag] += 1;

        user_tags[link.creator] = user_tags[link.creator] || {};
        user_tags[link.creator][tag] = user_tags[link.creator][tag] || 0;
        user_tags[link.creator][tag] += 1;
      }
    }
  } catch (error) {
    console.error(error);
    return res.status(500);
  } finally {
    for (const [url, count] of Object.entries(urls)) {
      redis.zadd(prefix + "_urls", count, url);
    }

    for (const [tag, count] of Object.entries(tags)) {
      redis.zadd(prefix + "_tags", count, tag);
    }

    for (const [uid, tags] of Object.entries(user_tags)) {
      for (const [tag, count] of Object.entries(tags)) {
        redis.zadd(prefix + "_tags_" + uid, count, tag);
      }
    }
  }
}

module.exports = {
  redisUpdate: redisUpdate,
};
