#!/usr/bin/env node

var http = require("http"),
  redisUpdate = require("./cron/redis").redisUpdate,
  i = 0,
  stamp = new Date().getTime(),
  dailyInterval = 24 * 60 * 60 * 1000;

var app = require("./server/app");

app.stamp = stamp;

setTimeout(redisUpdate, 60000);
setInterval(redisUpdate, dailyInterval);

// -- handle node exceptions
process.on("uncaughtException", function (err) {
  console.error(new Date().toString(), "uncaughtException", err.message);
  console.error(err.stack);
  process.exit(1);
});

// -- start server
http.createServer(app).listen(app.conf.port, function () {
  console.info(
    "Express server listening on port %d in %s mode",
    app.conf.port,
    app.settings.env
  );
});
