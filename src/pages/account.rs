use crate::routes::AppRoute;
use crate::types::User;
use yew::prelude::*;
use yew_router::components::RouterAnchor;

pub struct Account {
    props: Props,
    state: State,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    #[prop_or_default]
    pub viewer: Option<User>,
    pub on_viewer_save: Callback<User>,
}

pub struct State {
    viewer: Option<User>,
}

pub enum Msg {
    UpdateName(String),
    ViewerSave,
}

impl Component for Account {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let state = State {
            viewer: props.viewer.clone(),
        };
        Self { props, state, link }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::UpdateName(name) => {
                if let Some(mut viewer) = self.state.viewer.clone() {
                    viewer.name = name;
                    self.state.viewer = Some(viewer);
                }
                true
            }
            Msg::ViewerSave => match &self.state.viewer {
                Some(viewer) => {
                    self.props.on_viewer_save.emit(viewer.clone());
                    true
                }
                None => false,
            },
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.state.viewer = props.viewer.clone();
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        type Anchor = RouterAnchor<AppRoute>;

        let form = match &self.state.viewer {
            None => html! {},
            Some(viewer) => html! {
                <form onsubmit=self.link.callback(move |e: FocusEvent| {
                    e.prevent_default();
                    Msg::ViewerSave
                })>
                    <div class="field">
                        <label class="label">{"Name"}</label>
                        <div class="control">
                            <input
                                class="input"
                                type="text"
                                value=viewer.name
                                oninput=self.link.callback(|e: InputData| Msg::UpdateName(e.value))
                            />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">{"Email"}</label>
                        <div class="control">
                            <input
                                class="input is-static"
                                type="email"
                                value=viewer.email
                                readonly={true}
                            />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">{"Created"}</label>
                        <div class="control">
                            <input
                                class="input is-static"
                                type="text"
                                value=viewer.created
                                readonly={true}
                            />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">{"ID"}</label>
                        <div class="control">
                            <input
                                class="input is-static"
                                type="text"
                                value=viewer.user_id
                                readonly={true}
                            />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">{"Google ID"}</label>
                        <div class="control">
                            <input
                                class="input is-static"
                                type="text"
                                value=viewer.google_id
                                readonly={true}
                            />
                        </div>
                    </div>
                    <div class="field">
                        <label class="checkbox" disabled={true}>
                            <input type="checkbox" class="mr-2" checked={viewer.is_active} disabled={true} />
                            {"Active user"}
                        </label>
                    </div>
                    <div class="field">
                        <label class="checkbox" disabled={true}>
                            <input type="checkbox" class="mr-2" checked={viewer.is_admin} disabled={true} />
                            {"Admin user"}
                        </label>
                    </div>
                    <div>
                        <button type={"submit"} class="button is-success">{"Save"}</button>
                        <Anchor route=AppRoute::HomePage("".to_string()) classes="button is-light">{"Cancel"}</Anchor>
                    </div>
                </form>
            },
        };
        html! {
            <>
                <section class="section container">
                    <h1 class="title">{"User Account"}</h1>
                    {form}
                </section>
                <section class="section container">
                    <div class="columns">
                        <div class="column">
                            <h2 class="title is-4">{"Import"}</h2>
                            <p>{"Import bookmarks from an HTML bookmarks file"}</p>
                            <form method="post" action="/api/bookmarks" enctype="multipart/form-data">
                                <div class="file">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="import" />
                                        <span class="file-cta">
                                            <span class="file-label">{"Choose a file…"}</span>
                                        </span>
                                    </label>
                                </div>
                                <button class="button">{"Import"}</button>
                            </form>
                        </div>
                        <div class="column">
                            <h2 class="title is-4">{"Export"}</h2>
                            <p>{"Export your bookmarks as an HTML bookmraks file"}</p>
                            <a class="button" href="/api/bookmarks">{"Export"}</a>
                        </div>
                    </div>
                </section>
            </>
        }
    }
}
