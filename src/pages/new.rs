use crate::components::LinkAddSection;
use crate::types::{Link, TitleResponse, Url};
use yew::prelude::*;
use yew::services::console::ConsoleService;

pub struct New {
    props: Props,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub url: String,
}

impl Component for New {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <section class="section">
                <LinkAddSection
                    on_add_link=self.link.callback(|link: Link| Msg::AddLink(link))
                />
                <div style="display: flex; justify-content: space-around">
                    <a class="button is-link" href="/auth/google">{"Login with Google"}</a>
                </div>
            </section>
        }
    }
}
