use crate::api;
use crate::components::{Navbar, PublicLinks, UserLinks};
use crate::pages::{Account, Login};
use crate::routes::AppRoute;
use crate::types::{User, ViewerResponse};
use anyhow::Error;
use yew::format::Json;
use yew::prelude::*;
use yew::services::console::ConsoleService;
use yew::services::fetch::FetchTask;
use yew_router::prelude::*;

pub struct App {
    link: ComponentLink<Self>,
    state: State,
    task: Option<FetchTask>,
    current_route: Option<AppRoute>,
    #[allow(unused)]
    router_agent: Box<dyn Bridge<RouteAgent>>,
}

pub struct State {
    viewer: Option<User>,
}

pub enum Msg {
    GetViewer,
    GetViewerSuccess(ViewerResponse),
    GetViewerError(Error),
    SaveViewer(User),
    SaveViewerSuccess(ViewerResponse),
    SaveViewerError(Error),
    Route(Route),
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let state = State { viewer: None };
        link.send_message(Msg::GetViewer);
        let router_agent = RouteAgent::bridge(link.callback(Msg::Route));
        let route_service: RouteService = RouteService::new();
        let route = route_service.get_route();
        Self {
            link,
            task: None,
            state,
            current_route: AppRoute::switch(route),
            router_agent,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::Route(route) => {
                self.current_route = AppRoute::switch(route);
                true
            }
            Msg::GetViewer => {
                let handler =
                    self.link
                        .callback(move |response: api::FetchResponse<ViewerResponse>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(viewer) => Msg::GetViewerSuccess(viewer),
                                Err(err) => Msg::GetViewerError(err),
                            }
                        });
                self.task = Some(api::get_viewer(handler));
                true
            }
            Msg::GetViewerSuccess(viewer_response) => {
                ConsoleService::log(&format!("Viewer fetch got {:?}", viewer_response));
                self.state.viewer = Some(viewer_response.user);
                true
            }
            Msg::GetViewerError(error) => {
                ConsoleService::error(&format!("Viewer fetch error {:?}", error));
                false
            }
            Msg::SaveViewer(viewer) => {
                let handler =
                    self.link
                        .callback(move |response: api::FetchResponse<ViewerResponse>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(viewer) => Msg::SaveViewerSuccess(viewer),
                                Err(err) => Msg::SaveViewerError(err),
                            }
                        });
                self.task = Some(api::save_viewer(handler, viewer));
                true
            }
            Msg::SaveViewerSuccess(viewer_response) => {
                ConsoleService::log(&format!("Viewer saved got {:?}", viewer_response));
                self.state.viewer = Some(viewer_response.user);
                true
            }
            Msg::SaveViewerError(error) => {
                ConsoleService::error(&format!("Viewer save error {:?}", error));
                false
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let not_allowed = html! { "Not allowed. You need to log in first" };
        html! {
            <>
                <Navbar name={"Remember the link"} viewer={&self.state.viewer} />
                {
                    if let Some(route) = &self.current_route {
                        match route {
                            AppRoute::Account => {
                                match &self.state.viewer {
                                    None => not_allowed,
                                    Some(viewer) => html! {<Account viewer={viewer} on_viewer_save=&self.link.callback(|viewer: User| Msg::SaveViewer(viewer))/>}
                                }
                            }
                            AppRoute::Login => html! {<Login/>},
                            AppRoute::New(url) => {
                                match &self.state.viewer {
                                    None => not_allowed,
                                    Some(_) => html! {<UserLinks url=url tags="" />},
                                }
                            }
                            AppRoute::HomePage(tags) => {
                                match &self.state.viewer {
                                    None => html! {<PublicLinks/>},
                                    Some(_) => html! {<UserLinks url="" tags=tags />},
                                }
                            }
                        }
                    } else {
                        // 404 when route matches no component
                        html! { "No child component available" }
                    }
                }

            </>
        }
    }
}
