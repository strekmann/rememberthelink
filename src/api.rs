use crate::types::TagResponse;
use crate::types::User;
use crate::types::{
    DeleteResponse, Link, LinkResponse, PublicLinkResponse, TitleResponse, Url, ViewerResponse,
};
use anyhow::Error;
use serde_json::json;
use yew::callback::Callback;
use yew::format::{Json, Nothing};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

pub type FetchResponse<T> = Response<Json<Result<T, Error>>>;
type FetchCallback<T> = Callback<FetchResponse<T>>;

pub fn get_links(callback: FetchCallback<LinkResponse>, tags: String) -> FetchTask {
    let url = match tags.len() {
        0 => "/api/links/".to_string(),
        _ => format!("/api/links/{}", tags),
    };
    let req = Request::get(url).body(Nothing).unwrap();

    FetchService::fetch(req, callback).expect("Could not get links")
}

pub fn get_tags(callback: FetchCallback<TagResponse>, tags: String) -> FetchTask {
    let url = format!("/api/tags/{}", tags);
    let req = Request::get(url).body(Nothing).unwrap();

    FetchService::fetch(req, callback).expect("Could not get tags")
}

pub fn get_public_links(callback: FetchCallback<PublicLinkResponse>) -> FetchTask {
    let req = Request::get("/api/")
        .header("Content-Type", "application/json")
        .body(Nothing)
        .unwrap();

    FetchService::fetch(req, callback).expect("Could not get public links")
}

pub fn add_link(
    callback: FetchCallback<Link>,
    url: Url,
    title: String,
    description: String,
    tags: Vec<&str>,
    private: bool,
) -> FetchTask {
    let url = url.url;
    let body = &json!({ "url": url, "title": title, "tags": tags, "description": description, "private": private });
    let req = Request::post("/api/")
        .header("Content-Type", "application/json")
        .body(Json(body))
        .expect("Failed to build add-link request");

    FetchService::fetch(req, callback).expect("Could not get data from added link")
}

pub fn edit_link(
    callback: FetchCallback<Link>,
    link_id: String,
    url: Url,
    title: String,
    description: String,
    tags: Vec<&str>,
    private: bool,
) -> FetchTask {
    let url = url.url;
    let body = &json!({ "link_id": link_id, "url": url, "title": title, "tags": tags, "description": description, "private": private });
    let req = Request::patch("/api/")
        .header("Content-Type", "application/json")
        .body(Json(body))
        .expect("Failed to build update-link request");

    FetchService::fetch(req, callback).expect("Could not get data from added link")
}

pub fn delete_link(callback: FetchCallback<DeleteResponse>, link_id: usize) -> FetchTask {
    let body = &json!({ "link_id": link_id });
    let req = Request::delete("/api/")
        .header("Content-Type", "application/json")
        .body(Json(body))
        .expect("Failed to build delete-link request");

    FetchService::fetch(req, callback).expect("Could not get data from added link")
}

pub fn get_title(callback: FetchCallback<TitleResponse>, url: Url) -> FetchTask {
    let url = url.url;
    let req = Request::get(&format!("/api/title?url={}", url))
        .body(Nothing)
        .unwrap();

    FetchService::fetch(req, callback).expect("Could not get title")
}

pub fn get_viewer(callback: FetchCallback<ViewerResponse>) -> FetchTask {
    let req = Request::get("/api/account").body(Nothing).unwrap();
    FetchService::fetch(req, callback).unwrap()
}

pub fn save_viewer(callback: FetchCallback<ViewerResponse>, viewer: User) -> FetchTask {
    let body = &json!({"user_id": viewer.user_id, "email": viewer.email, "name": viewer.name});
    let req = Request::put("/api/account")
        .header("Content-Type", "application/json")
        .body(Json(body))
        .expect("Failed to build save-viewer request");
    FetchService::fetch(req, callback).unwrap()
}
