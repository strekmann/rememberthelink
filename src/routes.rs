use yew_router::prelude::*;

#[derive(Switch, Debug, Clone)]
pub enum AppRoute {
    #[to = "/account"]
    Account,
    #[to = "/login"]
    Login,
    #[to = "/new?url={*:named}"]
    New(String),
    #[to = "/{*}"]
    HomePage(String),
}
