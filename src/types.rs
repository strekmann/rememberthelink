use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Link {
    pub link_id: usize,
    pub url: String,
    pub title: Option<String>,
    pub description: Option<String>,
    pub tags: Vec<String>,
    pub created: DateTime<Utc>,
    pub private: bool,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct TitleResponse {
    pub url: String,
    pub title: String,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Tag {
    pub text: String,
    pub score: String,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct LinkResponse {
    pub links: Vec<Link>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct TagResponse {
    pub tags: Vec<Tag>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct PublicLinkResponse {
    pub urls: Vec<String>,
    pub tags: Vec<String>,
    pub url_count: usize,
    pub tag_count: usize,
    pub url: String,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Url {
    pub url: String,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct DeleteResponse {
    pub status: bool,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct ViewerResponse {
    pub user: User,
}

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct User {
    pub user_id: String,
    pub name: String,
    pub email: String,
    pub created: DateTime<Utc>,
    pub google_id: String,
    pub is_active: bool,
    pub is_admin: bool,
}
