use crate::api;
use crate::components::{Modal, ModalBody, ModalFooter, ModalHeader};
use crate::types::{Link, TitleResponse, Url};
use anyhow::Error;
use yew::format::Json;
use yew::prelude::*;
use yew::services::console::ConsoleService;
use yew::services::fetch::FetchTask;

pub struct LinkInput {
    pub id: String,
    pub url: String,
    pub title: String,
    pub description: String,
    pub tag_string: String,
    pub private: bool,
}

impl LinkInput {
    pub fn empty() -> Self {
        Self {
            id: String::from(""),
            url: String::from(""),
            title: String::from(""),
            description: String::from(""),
            tag_string: String::from(""),
            private: false,
        }
    }
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub url: Option<String>,
    pub on_add_link: Callback<Link>,
}

struct State {
    link: LinkInput,
    show_new_link_modal: bool,
}

pub struct LinkAddSection {
    state: State,
    link: ComponentLink<Self>,
    task: Option<FetchTask>,
    on_add_link: Callback<Link>,
}

pub enum Msg {
    AddLink,
    AddLinkSuccess(Link),
    AddLinkError(Error),
    AddLinkClose,
    GetTitle,
    GetTitleSuccess(TitleResponse),
    GetTitleError(Error),
    Update(String),
    UpdateDescription(String),
    TogglePrivate,
    UpdateTagString(String),
    UpdateTitle(String),
}

impl Component for LinkAddSection {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::log(&format!("props {:?}", props));
        let url = match &props.url {
            None => String::from(""),
            Some(url) => {
                link.send_message(Msg::GetTitle);
                url.clone()
            }
        };

        Self {
            state: State {
                link: LinkInput {
                    id: String::from(""),
                    description: String::from(""),
                    tag_string: String::from(""),
                    private: false,
                    title: String::from(""),
                    url,
                },
                show_new_link_modal: false,
            },
            on_add_link: props.on_add_link,
            link,
            task: None,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::AddLink => {
                let url = self.state.link.url.trim();
                if !url.is_empty() {
                    let url = Url { url: url.into() };
                    let handler = self
                        .link
                        .callback(move |response: api::FetchResponse<Link>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(link) => Msg::AddLinkSuccess(link),
                                Err(err) => Msg::AddLinkError(err),
                            }
                        });
                    self.task = Some(api::add_link(
                        handler,
                        url,
                        String::from(&self.state.link.title),
                        String::from(&self.state.link.description),
                        self.state
                            .link
                            .tag_string
                            .split(',')
                            .map(|tag| tag.trim())
                            .collect(),
                        self.state.link.private,
                    ));
                    return true;
                }
                false
            }
            Msg::AddLinkSuccess(link) => {
                self.state.show_new_link_modal = false;
                self.state.link = LinkInput::empty();
                self.on_add_link.emit(link);
                true
            }
            Msg::AddLinkError(error) => {
                ConsoleService::error(&format!("Data error {:?}", error));
                true
            }
            Msg::AddLinkClose => {
                self.state.link = LinkInput::empty();
                self.state.show_new_link_modal = false;
                true
            }
            Msg::TogglePrivate => {
                self.state.link.private = !self.state.link.private;
                true
            }
            Msg::Update(url) => {
                self.state.link.url = url;
                true
            }
            Msg::UpdateDescription(description) => {
                self.state.link.description = description;
                true
            }
            Msg::UpdateTagString(tag_string) => {
                self.state.link.tag_string = tag_string;
                true
            }
            Msg::UpdateTitle(title) => {
                self.state.link.title = title;
                true
            }
            Msg::GetTitle => {
                let url = self.state.link.url.trim();
                if !url.is_empty() {
                    let url = Url { url: url.into() };
                    let handler =
                        self.link
                            .callback(move |response: api::FetchResponse<TitleResponse>| {
                                let (_, Json(data)) = response.into_parts();
                                match data {
                                    Ok(link) => Msg::GetTitleSuccess(link),
                                    Err(err) => Msg::GetTitleError(err),
                                }
                            });
                    self.task = Some(api::get_title(handler, url));
                    return true;
                }
                false
            }
            Msg::GetTitleSuccess(link) => {
                self.state.show_new_link_modal = true;
                self.state.link.title = link.title;
                self.state.link.url = link.url;
                true
            }
            Msg::GetTitleError(error) => {
                ConsoleService::error(&format!("Title error {:?}", error));
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.on_add_link = props.on_add_link;
        true
    }

    fn view(&self) -> Html {
        html! {
            <section class="section">
                <div class="container">
                    <div class="columns">
                        <div class="column is-half is-offset-one-quarter">
                            <form onsubmit=self.link.callback(move |e: FocusEvent| {
                                e.prevent_default();
                                Msg::GetTitle
                            })>
                                <div class="field has-addons has-addons-centered">
                                    <div class="control is-expanded">
                                        <input
                                            autofocus={true}
                                            tabindex=1
                                            class="input"
                                            type="url"
                                            placeholder="https://new.link/"
                                            value=&self.state.link.url
                                            oninput=self.link.callback(|e: InputData| Msg::Update(e.value))
                                        />
                                    </div>
                                    <div class="control">
                                        <button type={"submit"} class="button is-link">{"Add"}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form onsubmit=self.link.callback(move |e: FocusEvent| {
                        e.prevent_default();
                        Msg::AddLink
                    })>
                        <Modal is_active={self.state.show_new_link_modal}>
                            <ModalHeader title="Add link" />
                            <ModalBody>
                                <div class="field">
                                    <label class="label">{"URL"}</label>
                                    <div class="control">
                                        <input
                                            readonly={true}
                                            class="input"
                                            type="url"
                                            placeholder="https://link.com/"
                                            value=&self.state.link.url
                                            oninput=self.link.callback(|e: InputData| Msg::Update(e.value))
                                        />
                                    </div>
                                </div>
                                <div class="field">
                                    <label class="label">{"Title"}</label>
                                    <div class="control">
                                        <input
                                            class="input"
                                            type="text"
                                            value=&self.state.link.title
                                            oninput=self.link.callback(|e: InputData| Msg::UpdateTitle(e.value))
                                        />
                                    </div>
                                </div>
                                <div class="field">
                                    <label class="label">{"Description"}</label>
                                    <div class="control">
                                        <textarea
                                            class="input"
                                            type="text"
                                            oninput=self.link.callback(|e: InputData| Msg::UpdateDescription(e.value))
                                        >{&self.state.link.description}</textarea>
                                    </div>
                                </div>
                                <div class="field">
                                    <label class="label">{"Comma separated tags"}</label>
                                    <div class="control">
                                        <input
                                            class="input"
                                            type="text"
                                            value=&self.state.link.tag_string
                                            oninput=self.link.callback(|e: InputData| Msg::UpdateTagString(e.value))
                                        />
                                    </div>
                                </div>
                                <div class="field">
                                    <label class="checkbox">
                                        <input type="checkbox" class="mr-2" onclick=self.link.callback(|e: MouseEvent| Msg::TogglePrivate) checked={self.state.link.private} />
                                        {"Private"}
                                    </label>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <button type={"submit"} class="button is-success">{"Save"}</button>
                                <a class="button is-light" onclick=self.link.callback(|e: MouseEvent| {Msg::AddLinkClose})>{"Cancel"}</a>
                            </ModalFooter>
                        </Modal>
                    </form>
                </div>
            </section>
        }
    }
}
