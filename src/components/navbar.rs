use crate::routes::AppRoute;
use crate::types::User;
use yew::prelude::*;
use yew_router::components::RouterAnchor;

pub struct Navbar {
    props: Props,
    state: State,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub name: String,
    pub viewer: Option<User>,
}

pub struct State {
    is_active: bool,
}

pub enum Msg {
    ToggleMenu,
}

impl Component for Navbar {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let state = State { is_active: false };
        Self { props, state, link }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::ToggleMenu => {
                self.state.is_active = match self.state.is_active {
                    false => true,
                    true => false,
                };
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        type Anchor = RouterAnchor<AppRoute>;

        let burger_class = match self.state.is_active {
            false => "navbar-burger",
            true => "navbar-burger is-active",
        };
        let menu_class = match self.state.is_active {
            false => "navbar-menu",
            true => "navbar-menu is-active",
        };
        let navbar_end = match &self.props.viewer {
            None => html! {
                    <Anchor route=AppRoute::Login classes="navbar-item">
                        {"Log in"}
                    </Anchor>
            },
            Some(viewer) => html! {
                <>
                    <Anchor route=AppRoute::Account classes="navbar-item">{&viewer.name}</Anchor>
                    <a href="/logout" class="navbar-item">{"Log out"}</a>
                </>
            },
        };

        html! {
            <nav class="navbar" role="navigation" aria-label="main-navigation">
                <div class="navbar-brand">
                    <Anchor route=AppRoute::HomePage("".to_string()) classes="navbar-item">
                        <h1 style={"margin: 0; font-size: 1.4rem;"}>{&self.props.name}</h1>
                    </Anchor>
                    <a role="button" class={burger_class} aria-label="menu" aria-expanded="false" onclick=self.link.callback(|_| Msg::ToggleMenu)>
                      <span aria-hidden="true"></span>
                      <span aria-hidden="true"></span>
                      <span aria-hidden="true"></span>
                    </a>
                </div>
                <div class={menu_class}>
                  <div class="navbar-start">
                  </div>

                  <div class="navbar-end">
                  {navbar_end}
                  </div>
                </div>
            </nav>

        }
    }
}
