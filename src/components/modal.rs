use yew::prelude::*;
use yew::services::console::ConsoleService;

pub struct Modal {
    props: Props,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub is_active: bool,
    pub children: Children,
}

impl Component for Modal {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        ConsoleService::log(&format!("Data {:?}", props));
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let modal_class = match self.props.is_active {
            true => "modal is-active",
            false => "modal",
        };
        html! {
            <div class={modal_class}>
                <div class="modal-background"></div>
                <div class="modal-card">
                    { self.props.children.clone() }
                </div>
            </div>
        }
    }
}
