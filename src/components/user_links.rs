use crate::api;
use crate::components::{LinkAddSection, LinkPanel, TagPanel};
use crate::types::TagResponse;
use crate::types::{Link, LinkResponse, Tag};
use anyhow::Error;
use yew::format::Json;
use yew::prelude::*;
use yew::services::fetch::FetchTask;

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub tags: String,
    pub url: Option<String>,
}

struct State {
    links: Vec<Link>,
    tags: Vec<Tag>,
    get_links_error: Option<Error>,
    get_links_loaded: bool,
    get_tags_error: Option<Error>,
    get_tags_loaded: bool,
    filter_tags: Vec<String>,
}

pub struct UserLinks {
    props: Props,
    state: State,
    link: ComponentLink<Self>,
    link_task: Option<FetchTask>,
    tag_task: Option<FetchTask>,
}

pub enum Msg {
    GetLinks,
    GetLinksSuccess(LinkResponse),
    GetLinksError(Error),
    GetTags,
    GetTagsSuccess(TagResponse),
    GetTagsError(Error),
    AddLink(Link),
    EditLink(Link),
    DeleteLink(String),
    AddFilterTag(String),
    RemoveFilterTag(String),
}

impl Component for UserLinks {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let links = vec![];
        let tags = vec![];
        let filter_tags = props.tags.split('/').map(|s: &str| s.to_string()).collect();

        link.send_message(Msg::GetLinks);
        link.send_message(Msg::GetTags);
        Self {
            props,
            state: State {
                links,
                tags,
                get_links_error: None,
                get_links_loaded: false,
                get_tags_error: None,
                get_tags_loaded: false,
                filter_tags,
            },
            link,
            link_task: None,
            tag_task: None,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::GetLinks => {
                self.state.get_links_loaded = false;
                let handler =
                    self.link
                        .callback(move |response: api::FetchResponse<LinkResponse>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(links) => Msg::GetLinksSuccess(links),
                                Err(err) => Msg::GetLinksError(err),
                            }
                        });
                self.link_task = Some(api::get_links(handler, self.state.filter_tags.join("/")));
                true
            }
            Msg::GetLinksSuccess(link_response) => {
                self.state.links = link_response.links;
                self.state.get_links_loaded = true;
                true
            }
            Msg::GetLinksError(error) => {
                self.state.get_links_error = Some(error);
                self.state.get_links_loaded = true;
                true
            }
            Msg::GetTags => {
                self.state.get_tags_loaded = false;
                let handler =
                    self.link
                        .callback(move |response: api::FetchResponse<TagResponse>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(tags) => Msg::GetTagsSuccess(tags),
                                Err(err) => Msg::GetTagsError(err),
                            }
                        });
                self.tag_task = Some(api::get_tags(handler, self.state.filter_tags.join("/")));
                true
            }
            Msg::GetTagsSuccess(tag_response) => {
                self.state.tags = tag_response.tags;
                self.state.get_tags_loaded = true;
                true
            }
            Msg::GetTagsError(error) => {
                self.state.get_tags_error = Some(error);
                self.state.get_tags_loaded = true;
                true
            }
            Msg::AddLink(link) => {
                self.state.links.insert(0, link);
                true
            }
            Msg::EditLink(link) => {
                self.state.links = self
                    .state
                    .links
                    .iter()
                    .map(|l: &Link| {
                        if link.link_id == l.link_id {
                            link.clone()
                        } else {
                            l.clone()
                        }
                    })
                    .collect();
                true
            }
            Msg::DeleteLink(link_id) => {
                let link_id: usize = link_id.parse().expect("link_id should be a string");
                self.state.links = self
                    .state
                    .links
                    .iter()
                    .filter(|l| link_id != l.link_id)
                    .cloned()
                    .collect();
                true
            }
            Msg::AddFilterTag(tag) => {
                self.state.filter_tags.push(tag);
                self.link.send_message(Msg::GetLinks);
                false
            }
            Msg::RemoveFilterTag(tag) => {
                self.state.filter_tags.retain(|t| t != &tag);
                self.link.send_message(Msg::GetLinks);
                false
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        if !self.state.get_tags_loaded || !self.state.get_links_loaded {
            html! {
                <div>{"Loading…"}</div>
            }
        } else {
            html! {
                <>
                    <LinkAddSection
                        url=&self.props.url
                        on_add_link=self.link.callback(|link: Link| Msg::AddLink(link))
                    />
                    <section class="section">
                        <div class="container">
                            <div class="columns">
                                <div class="column is-two-thirds">
                                    <LinkPanel
                                        links={&self.state.links}
                                        title="Newest links"
                                        filter_tags=&self.state.filter_tags
                                        on_edit_link=self.link.callback(|link: Link| Msg::EditLink(link))
                                        on_delete_link=self.link.callback(|link_id: String| Msg::DeleteLink(link_id))
                                        on_add_filter_tag=self.link.callback(|tag: String| Msg::AddFilterTag(tag))
                                        on_remove_filter_tag=self.link.callback(|tag: String| Msg::RemoveFilterTag(tag))
                                    />
                                </div>
                                <div class="column is-one-third">
                                    <TagPanel
                                        tags={&self.state.tags}
                                        title="Top tags"
                                        filter_tags=&self.state.filter_tags
                                        on_add_filter_tag=self.link.callback(|tag: String| Msg::AddFilterTag(tag))
                                        on_remove_filter_tag=self.link.callback(|tag: String| Msg::RemoveFilterTag(tag))
                                    />
                                </div>
                            </div>
                        </div>
                    </section>
                </>
            }
        }
    }
}
