use crate::api;
use crate::components::link_add_section::LinkInput;
use crate::components::{Modal, ModalBody, ModalFooter, ModalHeader, Tag};
use crate::types::{DeleteResponse, Link, Url};
use anyhow::Error;
use chrono::{DateTime, Utc};
use yew::format::Json;
use yew::prelude::*;
use yew::services::console::ConsoleService;
use yew::services::fetch::FetchTask;

pub struct LinkItem {
    props: Props,
    state: State,
    link: ComponentLink<Self>,
    task: Option<FetchTask>,
    on_edit_link: Callback<Link>,
    on_delete_link: Callback<String>,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub id: String,
    pub tags: Vec<String>,
    pub title: String,
    pub created: DateTime<Utc>,
    pub description: String,
    pub private: bool,
    pub url: String,
    pub filter_tags: Vec<String>,
    pub on_edit_link: Callback<Link>,
    pub on_delete_link: Callback<String>,
    pub on_add_filter_tag: Callback<String>,
    pub on_remove_filter_tag: Callback<String>,
}

struct State {
    link: LinkInput,
    show_edit_link_modal: bool,
}

pub enum Msg {
    DeleteLink,
    DeleteLinkSuccess(DeleteResponse),
    DeleteLinkError(Error),
    EditLink,
    EditLinkClose,
    EditLinkOpen,
    EditLinkSuccess(Link),
    EditLinkError(Error),
    UpdateTitle(String),
    UpdateDescription(String),
    TogglePrivate,
    UpdateTagString(String),
}

impl Component for LinkItem {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            task: None,
            props: props.clone(),
            state: State {
                show_edit_link_modal: false,
                link: LinkInput {
                    id: props.id,
                    title: props.title,
                    description: props.description,
                    tag_string: props.tags.join(", "),
                    private: props.private,
                    url: props.url,
                },
            },
            on_edit_link: props.on_edit_link,
            on_delete_link: props.on_delete_link,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::DeleteLink => {
                let handler =
                    self.link
                        .callback(move |response: api::FetchResponse<DeleteResponse>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(status) => Msg::DeleteLinkSuccess(status),
                                Err(err) => Msg::DeleteLinkError(err),
                            }
                        });
                self.task = Some(api::delete_link(
                    handler,
                    self.state
                        .link
                        .id
                        .parse()
                        .expect("Should be a string of int"),
                ));
                true
            }
            Msg::DeleteLinkSuccess(_) => {
                self.on_delete_link.emit(self.props.id.clone());
                true
            }
            Msg::DeleteLinkError(err) => {
                ConsoleService::error(&format!("Delete error {:?}", err));
                true
            }
            Msg::EditLink => {
                let handler = self
                    .link
                    .callback(move |response: api::FetchResponse<Link>| {
                        let (_, Json(data)) = response.into_parts();
                        match data {
                            Ok(link) => Msg::EditLinkSuccess(link),
                            Err(err) => Msg::EditLinkError(err),
                        }
                    });
                let url = Url {
                    url: self.state.link.url.trim().into(),
                };
                self.task = Some(api::edit_link(
                    handler,
                    String::from(&self.state.link.id),
                    url,
                    String::from(&self.state.link.title),
                    String::from(&self.state.link.description),
                    self.state
                        .link
                        .tag_string
                        .split(',')
                        .map(|tag| tag.trim())
                        .collect(),
                    self.state.link.private,
                ));
                true
            }
            Msg::EditLinkSuccess(link) => {
                self.state.show_edit_link_modal = false;
                self.state.link = LinkInput::empty();
                self.on_edit_link.emit(link);
                true
            }
            Msg::EditLinkError(error) => {
                ConsoleService::error(&format!("Data error {:?}", error));
                true
            }
            Msg::TogglePrivate => {
                self.state.link.private = !self.state.link.private;
                true
            }
            Msg::UpdateDescription(description) => {
                self.state.link.description = description;
                true
            }
            Msg::UpdateTagString(tag_string) => {
                self.state.link.tag_string = tag_string;
                true
            }
            Msg::UpdateTitle(title) => {
                self.state.link.title = title;
                true
            }
            Msg::EditLinkClose => {
                //self.state.link = self.link;
                self.state.show_edit_link_modal = false;
                true
            }
            Msg::EditLinkOpen => {
                //self.state.link = self.link;
                self.state.show_edit_link_modal = true;
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        // self.on_edit_link = props.on_edit_link;
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let tags: Vec<Html> = self
            .props
            .tags
            .iter()
            .map(|tag: &String| {
                let tag_toggled = self.props.filter_tags.iter().any(|t| tag == t);
                html! {
                    <Tag
                        text={tag}
                        is_toggled=tag_toggled
                        on_add_filter_tag=&self.props.on_add_filter_tag
                        on_remove_filter_tag=&self.props.on_remove_filter_tag
                    />
                }
            })
            .collect();
        let private = if self.props.private {
            html! {
                <div class="tag is-pulled-right">{"private"}</div>
            }
        } else {
            html! {}
        };
        let modal = if self.state.show_edit_link_modal {
            html! {
            <form onsubmit=self.link.callback(move |e: FocusEvent| {
                e.prevent_default();
                Msg::EditLink
            })>
                <Modal is_active={self.state.show_edit_link_modal}>
                    <ModalHeader title="Add link" />
                    <ModalBody>
                        <div class="field">
                            <label class="label">{"URL"}</label>
                            <div class="control">
                                <input
                                    readonly={true}
                                    class="input"
                                    type="url"
                                    placeholder="https://link.com/"
                                    value=&self.state.link.url
                                />
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">{"Title"}</label>
                            <div class="control">
                                <input
                                    class="input"
                                    type="text"
                                    value=&self.state.link.title
                                    oninput=self.link.callback(|e: InputData| Msg::UpdateTitle(e.value))
                                />
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">{"Description"}</label>
                            <div class="control">
                                <textarea
                                    class="input"
                                    type="text"
                                    oninput=self.link.callback(|e: InputData| Msg::UpdateDescription(e.value))
                                >{&self.state.link.description}</textarea>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">{"Comma separated tags"}</label>
                            <div class="control">
                                <input
                                    class="input"
                                    type="text"
                                    value=&self.state.link.tag_string
                                    oninput=self.link.callback(|e: InputData| Msg::UpdateTagString(e.value))
                                />
                            </div>
                        </div>
                        <div class="field">
                            <label class="checkbox">
                                <input type="checkbox" class="mr-2" onclick=self.link.callback(|e: MouseEvent| Msg::TogglePrivate) checked={self.state.link.private} />
                                {"Private"}
                            </label>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <button type={"submit"} class="button is-success">{"Save"}</button>
                        <a class="button is-light" onclick=self.link.callback(|e: MouseEvent| {Msg::EditLinkClose})>{"Cancel"}</a>
                    </ModalFooter>
                </Modal>
            </form>
            }
        } else {
            html! {}
        };

        html! {
            <div class="panel-block is-flex-direction-column is-align-items-stretch">
                {modal}
                <div class="columns mb-0">
                    <div class="column is-three-quarters">
                        <p class="is-size-5">{&self.props.title}</p>
                    </div>
                    <div class="column is-one-quarter">
                        <a class="button is-pulled-right" onclick=self.link.callback(|e: MouseEvent| { Msg::EditLinkOpen})>{"Edit"}</a>
                        <a class="button is-pulled-right" onclick=self.link.callback(|e: MouseEvent| { Msg::DeleteLink})>{"Delete"}</a>
                    </div>
                </div>
                <div class="columns mb-0">
                    <a class="column is-three-quarters is-clipped" style="white-space: nowrap; text-overflow: ellipsis;" href={String::from(&self.props.url)}>{&self.props.url}</a>
                    <div class="column is-one-quarter">
                        <time class="is-pulled-right" datetime={&self.props.created}>{&self.props.created.format("%F").to_string()}</time>
                    </div>
                </div>
                <div class="columns mb-0">
                    <div class="column is-three-quarters">{tags}</div>
                    <div class="column is-one-quarter">{private}</div>
                </div>
            </div>
        }
    }
}
