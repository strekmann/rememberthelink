use yew::prelude::*;

pub struct ModalBody {
    props: Props,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub children: Children,
}

impl Component for ModalBody {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <section class="modal-card-body">
                {self.props.children.clone()}
            </section>
        }
    }
}
