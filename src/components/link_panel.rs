use crate::components::LinkItem;
use crate::types::Link;
use yew::prelude::*;

pub struct LinkPanel {
    props: Props,
    link: ComponentLink<Self>,
    on_edit_link: Callback<Link>,
    on_delete_link: Callback<String>,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub links: Vec<Link>,
    pub title: String,
    pub filter_tags: Vec<String>,
    pub on_edit_link: Callback<Link>,
    pub on_delete_link: Callback<String>,
    pub on_add_filter_tag: Callback<String>,
    pub on_remove_filter_tag: Callback<String>,
}

pub enum Msg {
    EditLink(Link),
    DeleteLink(String),
}

impl Component for LinkPanel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            props: props.clone(),
            link,
            on_edit_link: props.on_edit_link,
            on_delete_link: props.on_delete_link,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::EditLink(link) => {
                self.on_edit_link.emit(link);
                true
            }
            Msg::DeleteLink(link_id) => {
                self.on_delete_link.emit(link_id);
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let links: Vec<Html> = self
            .props
            .links
            .iter()
            .map(|link: &Link| {
                html! {
                    <LinkItem
                        key={link.link_id.clone()}
                        id={&link.link_id.to_string()}
                        title={&link.title.clone().unwrap_or(String::from(""))}
                        url={&link.url}
                        created={&link.created}
                        description={&link.description.clone().unwrap_or(String::from(""))}
                        private={&link.private}
                        tags={&link.tags}
                        filter_tags={&self.props.filter_tags}
                        on_edit_link=self.link.callback(|link: Link| Msg::EditLink(link))
                        on_delete_link=self.link.callback(|link_id: String| Msg::DeleteLink(link_id))
                        on_add_filter_tag=&self.props.on_add_filter_tag
                        on_remove_filter_tag=&self.props.on_remove_filter_tag
                    />
                }
            })
            .collect();
        html! {
            <nav class="panel">
                <p class="panel-heading">{&self.props.title}</p>
                {links}
            </nav>
        }
    }
}
