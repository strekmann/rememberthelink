use yew::prelude::*;

pub struct Tag {
    props: Props,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub text: String,
    pub is_toggled: bool,
    pub on_add_filter_tag: Callback<String>,
    pub on_remove_filter_tag: Callback<String>,
}

pub enum Msg {
    ToggleFilterTag,
}

impl Component for Tag {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::ToggleFilterTag => {
                match self.props.is_toggled {
                    true => self
                        .props
                        .on_remove_filter_tag
                        .emit(self.props.text.clone()),
                    false => self.props.on_add_filter_tag.emit(self.props.text.clone()),
                }
                true
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let classes = match self.props.is_toggled {
            true => "tag mr-2 is-info is-light",
            false => "tag mr-2",
        };
        html! {
            <a
                href="#"
                class=classes
                onclick=self.link.callback(|e: MouseEvent| {e.prevent_default(); Msg::ToggleFilterTag})
            >
                {&self.props.text}
            </a>
        }
    }
}
