use yew::prelude::*;

pub struct ModalHeader {
    props: Props,
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub title: String,
}

impl Component for ModalHeader {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <header class="modal-card-head">
                <p class="modal-card-title">{&self.props.title}</p>
                <button class="delete" aria-label="close"></button>
            </header>
        }
    }
}
