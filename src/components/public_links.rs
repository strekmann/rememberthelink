use crate::api;
use crate::types::PublicLinkResponse;
use anyhow::Error;
use yew::format::Json;
use yew::prelude::*;
use yew::services::fetch::FetchTask;

pub struct PublicLinks {
    state: State,
    link: ComponentLink<Self>,
    task: Option<FetchTask>,
}

struct State {
    urls: Vec<String>,
    tags: Vec<String>,
    url_count: usize,
    tag_count: usize,
    url: String,
    get_links_error: Option<Error>,
    get_links_loaded: bool,
}

pub enum Msg {
    GetLinks,
    GetLinksSuccess(PublicLinkResponse),
    GetLinksError(Error),
}

impl Component for PublicLinks {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let urls = vec![];
        let tags = vec![];
        let url = String::from("localhost:3000");

        link.send_message(Msg::GetLinks);
        Self {
            state: State {
                urls,
                tags,
                get_links_error: None,
                get_links_loaded: false,
                url_count: 0,
                tag_count: 0,
                url,
            },
            link,
            task: None,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Msg::GetLinks => {
                self.state.get_links_loaded = false;
                let handler =
                    self.link
                        .callback(move |response: api::FetchResponse<PublicLinkResponse>| {
                            let (_, Json(data)) = response.into_parts();
                            match data {
                                Ok(links) => Msg::GetLinksSuccess(links),
                                Err(err) => Msg::GetLinksError(err),
                            }
                        });
                self.task = Some(api::get_public_links(handler));
                true
            }
            Msg::GetLinksSuccess(link_response) => {
                self.state.urls = link_response.urls;
                self.state.tags = link_response.tags;
                self.state.url_count = link_response.url_count;
                self.state.tag_count = link_response.tag_count;
                self.state.url = link_response.url;
                self.state.get_links_loaded = true;
                true
            }
            Msg::GetLinksError(error) => {
                self.state.get_links_error = Some(error);
                self.state.get_links_loaded = true;
                true
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let bookmarklet_link = format!(
            "javascript:var l=window.location.href;window.location.href='{}new?url='+l",
            &self.state.url
        );
        let urls: Vec<Html> = self
            .state
            .urls
            .iter()
            .map(|url: &String| {
                html! {
                    <li><a href={url.clone()}>{url}</a></li>
                }
            })
            .collect();
        let tags: Vec<Html> = self
            .state
            .tags
            .iter()
            .map(|tag: &String| {
                html! {
                    <li>{tag}</li>
                }
            })
            .collect();
        html! {
            <>
                <section class="hero is-light">
                    <div class="hero-body">
                        <div class="container">
                            <div class="columns is-align-content-space-between">
                                <div class="column is-two-thirds">
                                    <h1 class="title is-1">
                                        {"Remember the link"}
                                    </h1>
                                    <h2 class="subtitle is-1 has-text-grey-light">
                                        {"A place to store and share links"}
                                    </h2>
                                </div>
                                <div class="column is-one-third">
                                    <img class="is-block is-pulled-right" src="/img/screenshot.png" alt="screenshot" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container">
                    <div class="bubbles container my-5" style="max-width: 640px">
                        <div class="has-text-centered">
                            <img src="/img/bubbles.png" />
                        </div>
                        <div class="columns">
                            <div class="column has-text-centered">
                                <p>{"Add"}</p>
                            </div>
                            <div class="column has-text-centered">
                                <p>{"Tag"}</p>
                            </div>
                            <div class="column has-text-centered">
                                <p>{"Share"}</p>
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <p>{"Rememberthelink is a web tool where you can save, tag and share the links you find interesting, important or otherwise want to remember."}</p>
                        </div>
                        <div class="column">
                            <p>
                                {"We develop openly at"}
                                {" "}
                                <a href="https://github.com/strekmann/rememberthelink">{"Github"}</a>
                                {", you can install Rememberthelink on your own server, and we make sure our export and import scripts are always tested."}
                            </p>
                        </div>
                        <div class="column">
                            <p>{"We will admit we started this project because we were a bit unhappy about similar tools we knew about. And we did it to learn Node.js better."}</p>
                        </div>
                    </div>
                </section>
                <section class="container">
                    <div class="block">
                        <div class="columns">
                            <div class="column">
                                <h2 class="title is-5">{"Popular tags"}</h2>
                                <ul>
                                    {tags}
                                </ul>
                                <p>
                                    {"Tags stored:"}
                                    {" "}
                                    {&self.state.tag_count}
                                </p>
                            </div>
                            <div class="column">
                                <h2 class="title is-5">{"Popular urls"}</h2>
                                <ul>
                                    {urls}
                                </ul>
                                <p>
                                    {"Unique urls stored:"}
                                    {" "}
                                    {&self.state.url_count}
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container">
                    <div class="columns">
                        <div class="column">
                            <h3 class="title is-5">{"Bookmarklet"}</h3>
                            <p>{"Drag the button to your browser toolbar"}</p>
                            <a class="button" href={bookmarklet_link}>{"Remember"}</a>
                        </div>
                        <div class="column">
                            <h3 class="title is-5">{"Chrome extension"}</h3>
                            <p>
                                {"Download from"}
                                {" "}
                                <a href="https://chrome.google.com/webstore/detail/remember-the-link/ncapgeledeiafgomdafmafggglddijmf">{"google.com"}</a>
                            </p>
                        </div>
                        <div class="column">
                            <h3 class="title is-5">{"Firefox extension"}</h3>
                            <p>
                                {"Download from"}
                                {" "}
                                <a href="https://addons.mozilla.org/en-US/firefox/addon/remember-the-link/">{"mozilla.org"}</a>
                            </p>
                        </div>
                    </div>
                </section>
               <footer>
                    <div class="content has-text-centered">
                        <a href="https://github.com/strekmann/rememberthelink">{"Github"}</a>
                    </div>
               </footer>
            </>
        }
    }
}
