use crate::components::Tag as TagComponent;
use crate::types::Tag;
use yew::prelude::*;

pub struct TagPanel {
    props: Props,
}

#[derive(Properties, Clone, Debug)]
pub struct Props {
    pub tags: Vec<Tag>,
    pub title: String,
    pub filter_tags: Vec<String>,
    pub on_add_filter_tag: Callback<String>,
    pub on_remove_filter_tag: Callback<String>,
}

impl Component for TagPanel {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let tags: Vec<Html> = self
            .props
            .tags
            .iter()
            .map(|tag: &Tag| {
                let tag_toggled = self.props.filter_tags.iter().any(|t| &tag.text == t);
                html! {
                    <div class="panel-block">
                        <span class="tag mr-2">{&tag.score}</span>
                        {" "}
                        <TagComponent
                            text=&tag.text
                            is_toggled=tag_toggled
                            on_add_filter_tag=&self.props.on_add_filter_tag
                            on_remove_filter_tag=&self.props.on_remove_filter_tag
                        />
                    </div>
                }
            })
            .collect();
        html! {
            <nav class="panel">
                <p class="panel-heading">
                    {&self.props.title}
                </p>
                {tags}
            </nav>
        }
    }
}
